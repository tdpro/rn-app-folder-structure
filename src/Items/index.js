import React, {Component} from 'react'
import ItemsView from './ItemsView.js'
import AddNewItemView from './AddNewItemView.js'
import { createStackNavigator } from 'react-navigation-stack'
import { Drawer } from 'native-base'
export default (Drawnav = createStackNavigator(
  {
    ItemsView: {screen: ItemsView},
    AddNewItemView: {screen: AddNewItemView}
  },

  {
    initialRouteName: 'ItemsView',
    // headerMode: 'none',
  },
))
// export default  ItemsView
