import React, {Component} from 'react'
import ItemsView from '../Items/index.js'
import OrderView from '../Order/index.js'
import ReportView from '../Report/index.js'
import SettingsView from '../Settings/index.js'
import InfoView from '../Info/index.js'
import {createAppContainer} from 'react-navigation'
import { createDrawerNavigator } from 'react-navigation-drawer';
import Sidebar from '../Sidebar/Sidebar.js';
const HomeRouter = createDrawerNavigator(
  {
    OrderView: {
      screen: OrderView,
    },
    ItemsView: {
      screen: ItemsView,
    },
    ReportView: {
      screen: ReportView,
    },
    SettingsView: {
      screen: SettingsView,
    },
    InfoView:{
      screen: InfoView
    },
  },
  {
    initialRouteName: 'OrderView',
    // headerMode: 'none',
    contentOptions: {
      activeTintColor: '#e91e63',
    },
    contentComponent: props => <Sidebar {...props} />,
  },
)
export default createAppContainer(HomeRouter)
