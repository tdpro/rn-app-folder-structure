import React from 'react';
import { Image, StatusBar, TouchableOpacity } from 'react-native'
import {
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Form,
  List,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text,
  Input,
  Item,
  ListItem,
  Accordion,
  Toast,
  Root
} from 'native-base'

import Slider from '@react-native-community/slider';
import { withNavigationFocus } from 'react-navigation'
import AsyncStorage from '@react-native-community/async-storage'
//import { GoogleSignin, statusCodes, GoogleSigninButton, } from '@react-native-community/google-signin';


import { Col, Row, Grid } from 'react-native-easy-grid'
class OrderView extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      orderItemList: [],
      

    }
  }

  componentDidMount = () => {
    const { navigation } = this.props
    navigation.addListener('willFocus', () => {
      //   GoogleSignin.configure();
      this.retrieveData()
      this.retrieveOrderData()
      this.retrieveReportData()
    })
  }
  


  render() {
    return (
      <Root>
        <Container>
          <Content>
            
            <Text>Hello</Text>
           
          </Content>
        </Container>
      </Root>
    )
  }
}

OrderView.navigationOptions = ({ navigation }) => ({
  header: (
    <Header style={{ backgroundColor: '#ffffff' }}>
      <StatusBar backgroundColor="#000000" barStyle="light-content" />
      <Left>
        <Button transparent onPress={() => navigation.openDrawer()}>
          <Icon style={{ color: '000000' }} name="menu" />
        </Button>
      </Left>
      <Body>
        <Title style={{ color: '000000' }}>Order</Title>
      </Body>
      <Right />
    </Header>
  ),
})
export default withNavigationFocus(OrderView)