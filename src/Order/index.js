import React, {Component} from 'react'
import OrderView from './OrderView.js'
import { createStackNavigator } from 'react-navigation-stack'
import { Drawer } from 'native-base'
import SignIn from './SignIn'
import FileSystem from  './react-nativeFS/createFile'
import GoogleDrive from './react-nativeFS/drive'
import DriveExample from './react-nativeFS/GDriveWrapperExample/example'
export default (Drawnav = createStackNavigator(
  {
    OrderView: {screen: OrderView},
    SignIn: {screen:SignIn},
    GoogleDrive: {screen: GoogleDrive},
    DriveExample:{screen:DriveExample}
  },

  {
    initialRouteName: 'OrderView',
    // headerMode: 'none',
  },
))
// export default  OrderView
